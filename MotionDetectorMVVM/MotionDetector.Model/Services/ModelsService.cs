﻿using Emgu.CV;
using MotionDetector.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotionDetector.Model.Services
{
    public static class ModelsService
    {
        public static void SaveModels(List<Mat> models)
        {
            for (int i = 0; i < models.Count; i++)
            {
                var bmp = models[i].Bitmap;
                var fileName = Misc.MODELS_FILE_NAME + i.ToString() + ".png";
                bmp.Save(fileName);
            }
        }

        public static void SaveModelsCount(List<Mat> models, List<int> modelsCount)
        {
            var countFile = new StreamWriter(Misc.MODELS_FILE_NAME + ".txt");
            for (int i = 0; i < models.Count; i++)
            {
                countFile.WriteLine(modelsCount[i]);
            }
            countFile.Close();
        }
    }
}
