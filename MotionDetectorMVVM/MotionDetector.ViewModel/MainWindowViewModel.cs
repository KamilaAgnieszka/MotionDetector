﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.XFeatures2D;
using Microsoft.Win32;
using MotionDetector.Core;
using NLog;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace MotionDetector.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public VideoCapture capture;
        private DispatcherTimer playTimer = new DispatcherTimer();
        private Stopwatch watch = new Stopwatch();
        private int frameIndex;
        private Mat frame;
        private int framesCount;
        private Mat refFrame;
        private VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
        private List<Mat> descriptors = new List<Mat>();

        #region Commands

        public ICommand SelectFileCommand { get; set; }
        public ICommand StartCommand { get; set; }
        public ICommand PauseCommand { get; set; }
        public ICommand StopCommand { get; set; }
        public ICommand ComputeDescriptorsCommand { get; set; }
        public ICommand PrepareObjectPropertiesCommand { get; set; }

        #endregion

        #region Properties
        public int FrameNumber { get; private set; } = 1;
        public bool IsFirstFrame { get; private set; } = true;
        public List<Mat> Models { get; private set; } = new List<Mat>();
        public List<int> ModelsCount { get; private set; } = new List<int>();

        public Action<List<Mat>, List<int>, System.Drawing.Point, VectorOfVectorOfPoint, Mat> PrepareObjectPropertiesAction;

        private string selectedFilePath = @"C:\Users\Kamila\Desktop\Films\MOV_1108.mp4";
        public string SelectedFilePath
        {
            get { return selectedFilePath; }
            set
            {
                if (selectedFilePath != value)
                {
                    selectedFilePath = value;
                    RaiseOnPropertyChanged("SelectedFilePath");
                }
            }
        }

        private double threshShift = 1;
        public double ThreshShift
        {
            get { return threshShift; }
            set
            {
                if (threshShift != value)
                {
                    threshShift = value;
                    RaiseOnPropertyChanged("ThreshShift");
                }
            }
        }

        private decimal match;
        public decimal Match
        {
            get { return match; }
            set
            {
                if (match != value)
                {
                    match = value;
                    RaiseOnPropertyChanged("Match");
                }
            }
        }

        private decimal distance = 1;
        public decimal Distance
        {
            get { return distance; }
            set
            {
                if (distance != value)
                {
                    distance = value;
                    RaiseOnPropertyChanged("Distance");
                }
            }
        }

        private double threshold = 4;
        public double Threshold
        {
            get { return threshold; }
            set
            {
                if (threshold != value)
                {
                    threshold = value;
                    RaiseOnPropertyChanged("Threshold");
                }
            }
        }

        private string objectType;
        public string ObjectType
        {
            get { return objectType; }
            set
            {
                if (objectType != value)
                {
                    objectType = value;
                    RaiseOnPropertyChanged("ObjectType");
                }
            }
        }

        private long ticksElapsed;
        public long TicksElapsed
        {
            get { return ticksElapsed; }
            set
            {
                if (ticksElapsed != value)
                {
                    ticksElapsed = value;
                    RaiseOnPropertyChanged("TicksElapsed");
                }
            }
        }

        private IImage finalFrameImage;
        public IImage FinalFrameImage
        {
            get { return finalFrameImage; }
            set
            {
                if (finalFrameImage != value)
                {
                    finalFrameImage = value;
                    RaiseOnPropertyChanged("FinalFrameImage");
                }
            }
        }

        private IImage binaryFrameImage;
        public IImage BinaryFrameImage
        {
            get { return binaryFrameImage; }
            set
            {
                if (binaryFrameImage != value)
                {
                    binaryFrameImage = value;
                    RaiseOnPropertyChanged("BinaryFrameImage");
                }
            }
        }

        private IImage referenceFrameImage;
        public IImage ReferenceFrameImage
        {
            get { return referenceFrameImage; }
            set
            {
                if (referenceFrameImage != value)
                {
                    referenceFrameImage = value;
                    RaiseOnPropertyChanged("ReferenceFrameImage");
                }
            }
        }

        #endregion

        public MainWindowViewModel()
        {
            SelectFileCommand = new DelegateCommand(SelectFile);
            StartCommand = new DelegateCommand(Start);
            PauseCommand = new DelegateCommand(Pause);
            StopCommand = new DelegateCommand(Stop);
            ComputeDescriptorsCommand = new DelegateCommand(ComputeDescriptors);
            PrepareObjectPropertiesCommand = new DelegateCommand<object>(PrepareObjectProperties);

            PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Threshold")
                {
                    if (frame != null)
                    {
                        ProcessFrame();
                    }
                }
            };

            playTimer.Tick += PlayTimer_Tick;
            LoadModels();
        }

        private void SelectFile()
        {
            var dlg = new OpenFileDialog();

            // Display OpenFileDialog by calling ShowDialog method 
            bool? result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                SelectedFilePath = dlg.FileName;
            }
        }

        private void Start()
        {
            if (SelectedFilePath != "")
            {
                #region if capture is not created, create it now
                try
                {
                    capture = new VideoCapture(SelectedFilePath);
                    capture.SetCaptureProperty(CapProp.FrameWidth, 640);
                    capture.SetCaptureProperty(CapProp.FrameHeight, 360);
                }
                catch (NullReferenceException excpt)
                {
                    MessageBox.Show(excpt.Message);
                    logger.Error(excpt.Message);
                }
            }

            #endregion

            if (capture != null)
            {
                playTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000 / (int)capture.GetCaptureProperty(CapProp.Fps));
                //if camera is NOT getting frames then start the capture and set button
                // Text to "Stop" for pausing capture
                playTimer.Start();
            }
        }

        private void Pause()
        {
            if (capture != null)
            {
                capture.Pause();
                if (!playTimer.IsEnabled)
                {
                    playTimer.Start();
                }
                else
                {
                    playTimer.Stop();
                }
            }
        }

        public void Stop()
        {
            capture.Stop();
            playTimer.Stop();
            MessageBox.Show("End");
        }

        private void ComputeDescriptors()
        {
            descriptors.Clear();
            for (int i = 0; i < Models.Count; i++)
            {
                var detector = new SURF(300);
                var keyPoints = new VectorOfKeyPoint();
                var descriptor = new Mat();
                var model = new Mat();
                Models[i].ConvertTo(model, DepthType.Cv8U);
                detector.DetectAndCompute(model, null, keyPoints, descriptor, false);
                descriptors.Add(descriptor);
            }
        }

        private void PlayTimer_Tick(object sender, EventArgs e)
        {
            var frame = capture.QueryFrame();
            if (frame == null)
            {
                playTimer.Stop();
                MessageBox.Show("End");
            }
            else
            {
                watch.Restart();
                VideoProcessingLoop(capture, frame);
                watch.Stop();
                TicksElapsed = watch.ElapsedMilliseconds;
            }
        }

        private void VideoProcessingLoop(VideoCapture capture, Mat rawImage1)
        {
            if (rawImage1 != null)
            {
                FrameNumber++;
                frameIndex++;

                if (IsFirstFrame)
                {
                    IsFirstFrame = false;
                }
                frame = rawImage1;
                ProcessFrame();
            }
            else
            {
                // Move to first frame
                capture.SetCaptureProperty(CapProp.PosFrames, 0);
                FrameNumber = 0;
            }
        }

        private void ProcessFrame()
        {
            try
            {
                CvInvoke.Resize(this.frame, this.frame, Misc.Resolution);
                var frame = this.frame.Clone();
                framesCount++;

                var finalFrame = frame.Clone();
                FinalFrameImage = finalFrame;

                #region Count RefFrame
                CvInvoke.CvtColor(frame, frame, ColorConversion.Bgr2YCrCb);

                if (refFrame == null)
                {
                    refFrame = new Mat();
                    frame.ConvertTo(refFrame, DepthType.Cv32F);
                    return;
                }

                CvInvoke.AccumulateWeighted(frame, refFrame, 1.0 / Misc.REF_FRAMES_COUNT);

                using (var refFrame = new Mat())
                {
                    this.refFrame.ConvertTo(refFrame, DepthType.Cv8U);
                    CvInvoke.AbsDiff(refFrame, frame, frame);

                    if (framesCount == Misc.REF_FRAMES_COUNT)
                    {
                        CvInvoke.CvtColor(refFrame, refFrame, ColorConversion.YCrCb2Bgr);
                        ReferenceFrameImage = refFrame;
                        framesCount = 0;
                    }
                }
                #endregion

                CvInvoke.CvtColor(frame, frame, ColorConversion.Bgr2Gray);

                #region Create binFrame
                var binFrame = new Mat();

                CvInvoke.Threshold(frame, binFrame, Threshold + ThreshShift, 255, ThresholdType.Binary);
                contours = DetectObjects(binFrame);
                binFrame.SetTo(new MCvScalar(0));
                CvInvoke.DrawContours(binFrame, contours, -1, new MCvScalar(255), -1);

                BinaryFrameImage = binFrame;

                var backgndFrame = frame.Clone();
                //cars are '0'
                CvInvoke.BitwiseNot(binFrame, binFrame);
                //only the background remains
                CvInvoke.BitwiseAnd(binFrame, backgndFrame, backgndFrame);
                //sum non zero pixels of image
                var sum = CvInvoke.Sum(backgndFrame);
                //sum.V0 - sum non zero pixels of image (background)/amount of non zero pixels (background) - brightmess
                ThreshShift = sum.V0 / CvInvoke.CountNonZero(binFrame);

                #endregion

                var maxMatch = 0;
                var corners = new VectorOfKeyPoint();
                var dstDescriptor = new Mat();
                SURF surf = new SURF(Misc.HESSIAN_THRESH);

                for (int j = 0; j < contours.Size; j++)
                {
                    var contour = contours[j];
                    var bBox = CvInvoke.BoundingRectangle(contour);
                    var vehicle = new Mat(finalFrame, bBox);

                    CvInvoke.CvtColor(vehicle, vehicle, ColorConversion.Bgr2Gray);
                    vehicle = ResizeModel(vehicle);
                    CvInvoke.Rectangle(finalFrame, bBox, new MCvScalar(0, 0, 255), 2);

                    surf.DetectAndCompute(vehicle, null, corners, dstDescriptor, false);

                    for (int i = 0; i < corners.Size; i++)
                    {
                        var corner = corners[i];
                        var rc = new Rectangle(bBox.Left + (int)corner.Point.X, bBox.Top + (int)corner.Point.Y, 0, 0);
                        rc.Inflate(3, 3);
                        CvInvoke.Rectangle(finalFrame, rc, new MCvScalar(255, 0, 0), -1);
                    }

                    for (int i = 0; i < descriptors.Count; i++)
                    {
                        var matches = new VectorOfVectorOfDMatch();
                        BFMatcher bfMatcher = new BFMatcher(DistanceType.L2);
                        bfMatcher.Add(descriptors[i]);

                        if (descriptors[i].Height == 0)
                            continue;

                        //look among 10 nearest neighbours for nearest mateches
                        bfMatcher.KnnMatch(dstDescriptor, matches, 10, null);

                        var numberOfNearestMatches = 0;
                        //vector of assignments.Size
                        for (int k = 0; k < matches.Size; k++)
                        {
                            var vectorOfMatch = matches[k];
                            for (int l = 0; l < vectorOfMatch.Size; l++)
                            {
                                var match = vectorOfMatch[l];
                                //distance value of element of vectorOfMatch<value in DistBx
                                if (match.Distance < (float)Distance)
                                {
                                    numberOfNearestMatches++;
                                }
                            }
                        }
                        //number of nearest matches   
                        if (numberOfNearestMatches > maxMatch)
                        {
                            maxMatch = numberOfNearestMatches;
                            ObjectType = Misc.AvaliableObjectTypes[i % 4];
                            Match = maxMatch;
                            //MatchBox.Value = maxMatch;
                            //MatchBox.Refresh();
                        }
                    }

                    CvInvoke.PutText(finalFrame, ObjectType, new System.Drawing.Point(bBox.X + bBox.Width, bBox.Y + bBox.Height), FontFace.HersheySimplex, 1, new MCvScalar(255, 255, 255), 2, LineType.AntiAlias, false);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                logger.Error(e.Message);
            }
        }

        private VectorOfVectorOfPoint DetectObjects(Mat detectionFrame)
        {
            var objects = new VectorOfVectorOfPoint();
            var contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(detectionFrame, contours, null, RetrType.External, ChainApproxMethod.ChainApproxNone);
            for (int i = 0; i < contours.Size; i++)
            {
                var contour = contours[i];
                double area = CvInvoke.ContourArea(contour);
                if (area > Misc.MIN_AREA)
                {
                    objects.Push(contour);
                }
            }
            return objects;
        }

        private Mat ResizeModel(Mat m)
        {
            var size = m.Size;
            var aspect = (double)size.Height / size.Width;
            var modelAspect = (double)Misc.Resolution.Height / Misc.Resolution.Width;
            var h = Models[0].Size.Height;
            var w = Models[0].Size.Width;

            if (aspect > modelAspect)
            {
                CvInvoke.Resize(m, m, new System.Drawing.Size((int)(h / aspect), h));
                CvInvoke.CopyMakeBorder(m, m, 0, 0, 0, w - (int)(h / aspect), BorderType.Constant);
            }
            else
            {
                CvInvoke.Resize(m, m, new System.Drawing.Size(w, (int)(w * aspect)));
                CvInvoke.CopyMakeBorder(m, m, 0, h - (int)(w * aspect), 0, 0, BorderType.Constant);
            }
            return m;
        }

        public void LoadModels()
        {
            var modelSize = new SizeF(Misc.Resolution.Width * Misc.MODEL_SCALE, Misc.Resolution.Height * Misc.MODEL_SCALE).ToSize();
            for (int i = 0; i < 8; i++)
            {
                var model = new Mat(modelSize, DepthType.Cv8U, Misc.NUMBER_OF_CHANNELS);
                model.SetTo(new MCvScalar(0));
                Models.Add(model);
                ModelsCount.Add(0);
            }

            for (int i = 0; i < Models.Count; i++)
            {
                var fileName = Misc.MODELS_FILE_NAME + i.ToString() + ".png";
                if (!File.Exists(fileName))
                    continue;

                Models[i] = CvInvoke.Imread(fileName);
                CvInvoke.CvtColor(Models[i], Models[i], ColorConversion.Bgr2Gray);
            }

            var countFileName = Misc.MODELS_FILE_NAME + ".txt";

            if (File.Exists(countFileName))
            {
                var countFile = new StreamReader(countFileName);

                for (int i = 0; i < Models.Count; i++)
                {
                    ModelsCount[i] = int.Parse(countFile.ReadLine());
                }
                countFile.Close();
            }
            ComputeDescriptors();
        }

        private void PrepareObjectProperties(object location)
        {
            PrepareObjectPropertiesAction(Models, ModelsCount, (System.Drawing.Point)location, contours, frame);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaiseOnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
