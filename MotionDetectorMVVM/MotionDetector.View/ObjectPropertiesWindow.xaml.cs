﻿using Emgu.CV;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace MotionDetector.View
{
    /// <summary>
    /// Interaction logic for ObjectPropertiesWindow.xaml
    /// </summary>
    public partial class ObjectPropertiesWindow : Window
    {
        private List<int> modelsCount;
        private List<Mat> models;
        public Mat RoiVehicle { get; set; }
        public int ModelIdx
        {
            get
            {
                var orientIdx = leftRB.IsChecked.Value ? 0 : 1;
                return objectTypeCMB.SelectedIndex + 4 * orientIdx;
            }
        }
        public ObjectPropertiesWindow(List<int> modelsCount, List<Mat> models)
        {
            InitializeComponent();
            RoiVehicle = new Mat();
            this.modelsCount = modelsCount;
            this.models = models;
        }

        private void AddBTN_Click(object sender, RoutedEventArgs e)
        {
            var n = modelsCount[ModelIdx];
            var weight = 1.0 / (n + 1);
            CvInvoke.AddWeighted(models[ModelIdx], n * weight, RoiVehicle, weight, 0, models[ModelIdx]);
            modelsCount[ModelIdx]++;
            modelImage.Image = models[ModelIdx];
        }

        private void OkBTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void objectTypeCMB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (objectTypeCMB.SelectedIndex >= 0)
            {
                modelImage.Image = models[ModelIdx];
                System.Diagnostics.Trace.WriteLine(ModelIdx);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vehicleImage.Image = RoiVehicle;
        }

        private void OrientationRB_Checked(object sender, RoutedEventArgs e)
        {
            if (objectTypeCMB.SelectedIndex >= 0)
            {
                modelImage.Image = models[ModelIdx];
                System.Diagnostics.Trace.WriteLine(ModelIdx);
            }
        }
    }
}