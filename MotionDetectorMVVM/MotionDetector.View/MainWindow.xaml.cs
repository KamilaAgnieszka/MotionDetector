﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using MotionDetector.Core;
using MotionDetector.Model.Services;
using MotionDetector.ViewModel;
using System.Windows;

namespace MotionDetector.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();

            MatchBox.ValueType = typeof(int);
            MatchBox.ValueConstraint = new Infragistics.Windows.Editors.ValueConstraint()
            {
                MaxInclusive = 2000000,
            };

            DataContext = viewModel = new MainWindowViewModel();

            viewModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "FinalFrameImage")
                {
                    (finalFrameIBHost.Child as Emgu.CV.UI.ImageBox).Image = viewModel.FinalFrameImage;
                }
                else if (e.PropertyName == "ReferenceFrameImage")
                {
                    (referenceFrameIBHost.Child as Emgu.CV.UI.ImageBox).Image = viewModel.ReferenceFrameImage;
                    (referenceFrameIBHost.Child as Emgu.CV.UI.ImageBox).Refresh();
                }
                else if (e.PropertyName == "BinaryFrameImage")
                {
                    (binaryFrameIBHost.Child as Emgu.CV.UI.ImageBox).Image = viewModel.BinaryFrameImage;
                }
            };

            viewModel.PrepareObjectPropertiesAction = (models, modelsCount, processingLocation, contours, frame) =>
            {
                var dialog = new ObjectPropertiesWindow(modelsCount, models);
                for (int i = 0; i < contours.Size; i++)
                {
                    var contour = contours[i];
                    var bBox = CvInvoke.BoundingRectangle(contour);
                    processingLocation.X = processingLocation.X * frame.Width / finalFrameIB.Width;
                    processingLocation.Y = processingLocation.Y * frame.Height / finalFrameIB.Height;

                    if (bBox.Contains(processingLocation))
                    {
                        var model = new Mat(frame, bBox);
                        CvInvoke.CvtColor(model, model, ColorConversion.Bgr2Gray);
                        dialog.RoiVehicle = ResizeModel(model);
                        break;
                    }
                }
                dialog.ShowDialog();
                ModelsService.SaveModels(viewModel.Models);
                ModelsService.SaveModelsCount(viewModel.Models, viewModel.ModelsCount);
            };
        }

        private Mat ResizeModel(Mat m)
        {
            var size = m.Size;
            var aspect = (double)size.Height / size.Width;
            var modelAspect = (double)Misc.Resolution.Height / Misc.Resolution.Width;
            var h = viewModel.Models[0].Size.Height;
            var w = viewModel.Models[0].Size.Width;

            if (aspect > modelAspect)
            {
                CvInvoke.Resize(m, m, new System.Drawing.Size((int)(h / aspect), h));
                CvInvoke.CopyMakeBorder(m, m, 0, 0, 0, w - (int)(h / aspect), BorderType.Constant);
            }
            else
            {
                CvInvoke.Resize(m, m, new System.Drawing.Size(w, (int)(w * aspect)));
                CvInvoke.CopyMakeBorder(m, m, 0, h - (int)(w * aspect), 0, 0, BorderType.Constant);
            }
            return m;
        }

        private void finalFrameIB_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            viewModel.PrepareObjectPropertiesCommand.Execute(e.Location);
        }
    }
}
