﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotionDetector.Core
{
    public class Misc
    {
        public const int REF_FRAMES_COUNT = 1000;
        public const float MODEL_SCALE = 0.3f;
        public const int MIN_AREA = 100;
        public const int NUMBER_OF_CHANNELS = 1;
        public const string MODELS_FILE_NAME = "Models";
        public const double HESSIAN_THRESH = 300;

        public static Size Resolution = new Size(640, 360);

        public static List<String> AvaliableObjectTypes = new List<string>()
        {
            "car",
            "truck",
            "bus",
            "van"
        };
    }
}
